cmake_minimum_required(VERSION 3.20)

install(FILES share/drivers_pmg DESTINATION /etc/init.d 
  PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)

install(CODE "execute_process(COMMAND sh -c \"chcon -u system_u -t initrc_exec_t /etc/init.d/drivers_pmg\" )")

install(FILES share/drivers_pmg.service DESTINATION /etc/systemd/system)
